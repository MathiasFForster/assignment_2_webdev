
NAME: HERNAN MATHIAS FARINA FORSTER
COURSE: 420-320-DW LINUX II
TITLE: ASSIGNMENT 5

OBJECTIVE: Containerize a JS application from the 320 course. Once done, it will be deployed using Netlify and Azure.

INSTRUCTIONS:
step 1: docker build -t app
step 2: run -dit --name web-assignment-app -p 8080:80 app
step 3: Show up in browser 10.172.16.206:8080

Netlify:
luxury-kangaroo-a76d90.netlify.app/html/assignment2.html 

Azure:
FQDN: mathiasforstercurrency.fvdpc2hkdwgne8d6.eastus.azurecontainer.io
IP: http://51.8.11.197/ 


 