"use strict";

//Array containg the objects added to the table.
let objects_array = [];

//Event Listener that calls the method that initializes the options when the website loads.
addEventListener('load', (event) => {
    initialize_Select_Options_data();
});

//Event Listener that call the getData function once clicked.
let convertButton = document.getElementById('get_data_btn');
convertButton.addEventListener('click',getData);

//Object containing the keys/values for the construction of the url (this object is null everytime the program starts) It also calls the absoluteURL function.
let urlObject = {
    mainURL: 'https://api.exchangerate.host/convert',
    fromQry: `?from=${document.getElementById("base_currency_select").value}&`,
    toQry: `to=${document.getElementById("to_currency_select").value}&`,
    amountQry: `amount=${document.getElementById("amount").value}`,
    absoluteURL: getAbsoluteURL
};

/**
 * It builds the url with the values got from both inputs (lists).
 * @returns the absolute url which contains the values that will later be added to the object array.
 */
function getAbsoluteURL() {
    urlObject.mainURL = 'https://api.exchangerate.host/convert';
    urlObject.fromQry = `?from=${document.getElementById("base_currency_select").value}&`;
    urlObject.toQry =  `to=${document.getElementById("to_currency_select").value}&`;
    urlObject.amountQry =  `amount=${document.getElementById("amount").value}`;
    return `${urlObject.mainURL}${urlObject.fromQry}${urlObject.toQry}${urlObject.amountQry}`;
}

/**
 * This function gets the absoluteURL and and goes through the information that will send as an input to the displayResults function.
 */
function getData() {
    let url = getAbsoluteURL();
    fetch(url) 
    .then((response) => response.json())
    .then((data) => displayResults(data))
    .catch((error) => console.log("An error has occurred. Error: " + error));
}
/**
 * Builds dynamically and displays the table with each cell containing the values obtained from the input (data)
 * @param {promise} data - The object promise containing the keys/values of the conversion.
 */

function displayResults(data) {

    let date = new Date;

    if(document.getElementById("trs") != null) {
        document.getElementById("trs").remove();
    }

    if(document.getElementById("ths") != null) {
        document.getElementById("ths").remove();
    }

    let arr_header = ['from','to','rate','amount','payment','date'];
    let table = document.getElementById('currency_table');
    let thead = document.createElement('thead');
    let tr_element = document.createElement("tr");
    tr_element.id = "trs";
    thead.id = "ths";

    table.appendChild(thead);
    thead.appendChild(tr_element);

    table.style.border = "1px dotted black";

    for (let index = 0; index < 6; index++) {
        let th_element = document.createElement("th");
        th_element.textContent = arr_header[index];
        tr_element.appendChild(th_element);
    }

    let tbody = document.createElement('tbody');
    tbody.id = 'table_body';

    if(document.getElementById('table_body') != null) {
        document.getElementById('table_body').remove();
    }

    let dataExtraction = new Object();
        dataExtraction.from = data['query'].from;
        dataExtraction.to = data['query'].to;
        dataExtraction.rate = data['info'].rate;
        dataExtraction.amount = data['query'].amount;
        dataExtraction.payment = data.result;
        dataExtraction.date = date.getFullYear() + '-' + (date.getMonth()+1) + '-' + date.getUTCDate() + '    -    ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();

    objects_array.push(dataExtraction);

    objects_array.forEach(conversion => {
        let tr_body = document.createElement('tr');
        for (let key in conversion) {
            let tds = document.createElement('td');
            tds.textContent = `${conversion[key]}`
            tr_body.appendChild(tds);
            tbody.appendChild(tr_body);
        }
    }) 
    table.appendChild(tbody);
}


