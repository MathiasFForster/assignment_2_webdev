"use strict"

//Relative path to the json type document containing the different currencies.
const url = "../json/currency.json";

/**
 * This function initilizaties the differents options containng the names of the currencies by fetching the currency.json document,
 * creates a new element and add to the list. It also stablishes the value for each option (the code of the currency chosen).
 */
function initialize_Select_Options_data() {

    let currencyName = "";
    let base_select = document.getElementById("base_currency_select");
    let to_select = document.getElementById("to_currency_select");

    fetch(url)
    .then((response) => response.json())
    .then((data) => {
        let obj = data;
        for(let i in obj) {
            let newOption = document.createElement("option");
            currencyName = obj[i].name;
            newOption.textContent = currencyName;
            newOption.value = obj[i].code;
            base_select.appendChild(newOption);
        }   
    }); 

    fetch(url)
    .then((response) => response.json())
    .then((data) => {
        let obj = data;
        for(let i in obj) {
            let newOption = document.createElement("option");
            currencyName = obj[i].name;
            newOption.textContent = currencyName;
            newOption.value = obj[i].code;
            to_select.appendChild(newOption);
        }  
    }); 
}



    

